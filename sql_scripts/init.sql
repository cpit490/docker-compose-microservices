
-- Create database i f not exist
CREATE DATABASE IF NOT EXISTS demo_db;

CREATE TABLE IF NOT EXISTS product  (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    price DECIMAL
);

CREATE TABLE IF NOT EXISTS review (
    id SERIAL PRIMARY KEY,
    product_id BIGINT UNSIGNED,
    rating INT,
    comment TEXT,
    FOREIGN KEY (product_id) REFERENCES product(id)
);

INSERT INTO product (name, price) VALUES
    ('Table', 100),
    ('Chair', 50),
    ('Sofa', 200),
    ('Bed', 150),
    ('Wardrobe', 300),
    ('Bookshelf', 80),
    ('Dresser', 120),
    ('Desk', 90),
    ('Cabinet', 70),
    ('Bench', 40);


INSERT INTO review (product_id, rating, comment) VALUES
    (1, 5, 'Great table!'),
    (1, 4, 'Good quality'),
    (1, 5, 'Would buy again'),
    (2, 3, 'Average chair'),
    (2, 4, 'Comfortable'),
    (2, 3, 'Could be better'),
    (3, 5, 'Excellent sofa'),
    (3, 5, 'Very comfortable'),
    (3, 4, 'Good value for money'),
    (4, 5, 'Best sleep ever'),
    (4, 4, 'Good quality'),
    (4, 5, 'Highly recommended'),
    (5, 4, 'Spacious wardrobe'),
    (5, 3, 'Difficult assembly'),
    (5, 4, 'Good value'),
    (6, 5, 'Perfect for books'),
    (6, 4, 'Good quality'),
    (6, 5, 'Would buy again'),
    (7, 3, 'Average dresser'),
    (7, 4, 'Good storage space'),
    (7, 3, 'Could be better'),
    (8, 5, 'Great for working'),
    (8, 5, 'Very comfortable'),
    (8, 4, 'Good value for money'),
    (9, 4, 'Useful cabinet'),
    (9, 3, 'Average quality'),
    (9, 4, 'Good for storage'),
    (10, 5, 'Sturdy bench'),
    (10, 4, 'Good quality'),
    (10, 5, 'Highly recommended');
