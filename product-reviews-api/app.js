const express = require('express');
const cors = require('cors');
const mysql = require('mysql2/promise');

const port = process.env.API_SERVER_PORT || 45000;


const pool = mysql.createPool({
    host: process.env.MariaDB_HOST || 'db',
    user: process.env.MariaDB_USER || 'root',
    password: process.env.MariaDB_PASSWORD || 'changeme',
    database: process.env.MariaDB_DATABASE || 'demo_db',
});

const app = express();

app.use(cors());

app.get('/products', async (req, res) => {
    const [rows] = await pool.query('SELECT * FROM product');
    res.json(rows);
});

app.get('/products/:id', async (req, res) => {
    const [rows] = await pool.query('SELECT * FROM product WHERE id = ?', [req.params.id]);
    res.json(rows[0]);
});

app.get('/reviews', async (req, res) => {
    const [rows] = await pool.query('SELECT * FROM review');
    res.json(rows);
});

app.get('/products/:id/reviews', async (req, res) => {
    const [rows] = await pool.query('SELECT * FROM review WHERE product_id = ?', [req.params.id]);
    res.json(rows);
});

app.listen(port, '0.0.0.0', () => {
    console.log('Server is running on port', port);
});